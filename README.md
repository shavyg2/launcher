#Launch
-----------------------

Launcher is made to launch application. This will manage a process and makes sure that it stays alive.

Launch functions in two different ways.

The first thing launcher does is check if a process is running. This will allow it to attached to that process and to watch and keep that process running.

The second thing launcher will do is check if a process is running.

Currently Launch can manage two different applications. How this is configured through the command line will however change.

##*Example*

Issue Launcher attempts to solve. 
You have an Application that needs to run but you also need an Updater application to run that will update this application.

Launcher will allow your application to run and it won't restart your application if the updater is running. This allows your application to update and once the updater is completed your updater can either launch your application or launcher will understand that the updater and the application is not running so to start running the application.

	PanaceaLauncher.exe --software path/to/Software.exe

The path to the software can be relative or absolute paths to the current working directory.

Currently Launcher is configured to work with a specific updater but as it evolves this will be included into the command line.

Launchers aim is to be diverse and not only work for an application but to work with a series of Operations.

As features are add they will be updated.



