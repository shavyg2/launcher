﻿using Commander;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PanaceaLauncher
{
    class Program
    {
        public static string process_name;
        public static string software;
        public static Process process;
        public static Process IbtUpdater;
        public static bool isWatching = false;

        public static bool restart = false;

        static void Main(string[] args)
        {
            dynamic cmd = new Command("1.0.0");
            cmd.parse(args);
            cmd.set("s", "software", "Use this to set the software you want to run forever", true);

            if (cmd.software != null)
            {
                software = cmd.software;
                LoadApplication(cmd.software);
            }
        }


        public static void LoadApplication(string software){
            
            log("Loading Application");

            process_name = Path.GetFileNameWithoutExtension(software);
            var processes=Process.GetProcessesByName(process_name);
            if (processes != null && processes.Length > 0)
            {
                process = processes[0];
                watch_process();
            }
            else {
                StartProcess(software);
            }
        }

        public static void log(params object[] objs) {
            Console.WriteLine(objs);
        }

        public static void log(string log, params object[] objs) {
            Console.WriteLine(log, objs);
        }

        public static void log(string log) {
            Console.WriteLine(log);
        }

        public static void StartProcess(string path) {
            if(!UpdaterRunning())
            {
                log("Starting Application");
                restart = false;
                process = new Process();
                var ps = new ProcessStartInfo();
                ps.FileName = path;
                process.StartInfo = ps;
                process.Start();
                watch_process();
            }
        }

        public static bool UpdaterRunning() {
            var processes = Process.GetProcessesByName("IBT.Updater");
            return processes != null && processes.Length > 0;
        }

        public static void watch_process() {
            new Thread(new ParameterizedThreadStart(watcher)).Start(process);
        }

        public static void watcher(object obj) {
            if (!isWatching)
            {
                isWatching = !isWatching;
                var p = (Process)obj;
                log("Watching the Process");
                while (true)
                {
                    try
                    {
                        var reset = process.HasExited;
                        if (reset)
                        {
                            StartProcess(software);
                        }
                    }
                    catch (Exception e) { }

                    Thread.Sleep(5000);
                }
            }
        }
    }
}
